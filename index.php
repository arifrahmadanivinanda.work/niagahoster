<?php
$json_price = file_get_contents('http://high-keyed-rate.000webhostapp.com/json/');
$price = json_decode($json_price);
?>

<html>
    <head>
        <meta charset="UTF-8" />
        <title>
            Web Hosting Indonesia Unlimited & Terbaik - Niagahoster
        </title>

        <!-- Bootstrap 4.3.1 -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
    
        <!-- Import Font -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <style>
            @font-face {
                font-family: roboto-regular;
                src: url(assets/fonts/roboto/Roboto-Regular.ttf);
            }
            @font-face {
                font-family: roboto-regular;
                src: url(assets/fonts/roboto/Roboto-Bold.ttf);
                font-weight: bold;
            }
            @font-face {
                font-family: montserrat-bold;
                src: url(assets/fonts/montserrat/Montserrat-Bold.otf) format("opentype");;
            }
            @font-face {
                font-family: montserrat-light;
                src: url(assets/fonts/montserrat/Montserrat-Light.otf) format("opentype");;
            }
            @font-face {
                font-family: montserrat-light;
                src: url(assets/fonts/montserrat/Montserrat-Bold.otf) format("opentype");;
                font-weight: bold;
            }
        </style>
    </head>
    
    <body bgcolor="#ffffff">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <a href="#"><img src="assets/images/tag.png" width="25px%"></a>
                    <a href="#" style="color:black;font-family: roboto-regular;font-size:0.8em">&nbsp;Gratis Ebook 9 Cara Cerdas Menggunakan Domain</a>
                    <a href="#" style="color:black;font-size:0.8em">[ x ]</a>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12" style="padding-top:10px;font-size:0.9em;text-align:right">
                    <span class="glyphicon glyphicon-earphone"></span> <a href="#" style="color:black;font-family: roboto-regular">0274-5305505</a>&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-comment"></span> <a href="#" style="color:black;font-family: roboto-regular">Live Chat</a>&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-user"></span> <a href="#" style="color:black;font-family: roboto-regular">Member Area</a>
                </div>
            </div>
        </div>
        <hr/>
        <div class="container">
            <nav class="navbar navbar-light navbar-expand-lg">
                <a class="navbar-brand" href="#"><img src="assets/images/logo.png" height="60" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"></li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item" style="padding-right:15px">
                            <a class="nav-link active" aria-current="page" href="#">Hosting</a>
                        </li>
                        <li class="nav-item" style="padding-right:15px">
                            <a class="nav-link" href="#">Domain</a>
                        </li>
                        <li class="nav-item" style="padding-right:15px">
                            <a class="nav-link" href="#">Server</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Website</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Afiliasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Promo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Pembayaran</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Review</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="padding-right:15px">Kontak</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Blog</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <hr/>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <span style="font-family:montserrat-bold;font-size:4em">
                        PHP Hosting
                    </span>
                    <br/>
                    <span style="font-family:montserrat-light;font-size:2em">
                        Cepat, handal, penuh dengan <br/> modul PHP yang Anda butuhkan
                    </span>
                    <br/>
                    <br/>
                    <table>
                        <tr>
                            <td><img src="assets/images/check.png" width="20vw"></td>
                            <td><span style="font-family:montserrat-light;font-size:1em">&nbsp;Solusi PHP untuk performa query yang lebih cepat. <span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:1em">&nbsp;Konsumsi memori yang lebih rendah. <span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:1em">&nbsp;Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7 <span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:1em">&nbsp;Fitur enkripsi IonCube dan Zend Guard Loaders <span></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    &nbsp;
                    <center><img src="assets/svg/illustration banner PHP hosting-01.svg" style="width:100%;max-width:400px"></center>
                </div>
            </div>
        </div>
        <br/>
        <hr/>
        <br/>
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-12"></div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <table style="width:100%">
                        <tr>
                            <td style="padding:30px"><center><img src="assets/svg/illustration-banner-PHP-zenguard01.svg" style="width:100%;max-width:200px"></center></td>
                            <td style="padding:30px"><center><img src="assets/svg/icon-composer.svg" style="width:100%;max-width:150px"></center></td>
                            <td style="padding:30px"><center><img src="assets/svg/icon-php-hosting-ioncube.svg" style="width:100%;max-width:200px"></center></td>
                        </tr>
                        <tr>
                            <td><center><span style="font-family:montserrat-light;font-size:1em">PHP Zend Guard Loader <span></center></td>
                            <td><center><span style="font-family:montserrat-light;font-size:1em">PHP Composer<span></center></td>
                            <td><center><span style="font-family:montserrat-light;font-size:1em">PHP IonCube Loader<span></center></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-2 col-md-1 col-sm-12"></div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><span style="font-family:montserrat-bold;font-size:2em">Paket Hosting Singapura yang Tepat</span></center>
                    <center><span style="font-family:montserrat-light;font-size:1.6em">Diskon 40% + Domain dan SSL Gratis untuk Anda<span></center>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12" style=" line-height: 1.8;margin-top:30px">
                    <div style="width:100%;line-height: 1.8;border:1px solid #e4e2e2; border-radius:5px;padding-bottom:20px">
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Bayi</span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em;"><strike>Rp <?php echo number_format($price->bayi_old); ?></strike><span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em">Rp.</span><span style="font-family:montserrat-bold;font-size:2em"><?php echo substr($price->bayi_new, 0,-3); ?></span><span style="font-family:montserrat-light;font-size:1em"><b>.<?php echo substr($price->bayi_new, -3); ?></b>/bln</span></center>
                    </center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>938</b> Pengguna Terdaftar<span></center>
                    <br/>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>0.5X RESOURCE POWER</b><span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>500 MB</b> Disk Space<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Bandwidth<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Database<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>1</b> Domain<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Instant</b> Backup<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited SSL</b> Gratis Selamanya<span></center>
                    </br>
                    <center><button class="btn btn-outline-dark" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Pilih Sekarang</span></button></center>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12" style=" line-height: 1.8;margin-top:30px">
                    <div style="width:100%;line-height: 1.8;border:1px solid #e4e2e2; border-radius:5px;padding-bottom:20px">
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Pelajar</span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em;"><strike>Rp <?php echo number_format($price->pelajar_old); ?></strike><span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em">Rp.</span><span style="font-family:montserrat-bold;font-size:2em"><?php echo substr($price->pelajar_new, 0,-3); ?></span><span style="font-family:montserrat-light;font-size:1em"><b>.<?php echo substr($price->pelajar_new, -3); ?></b>/bln</span></center>
                    </center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>4.168</b> Pengguna Terdaftar<span></center>
                    <br/>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>1X RESOURCE POWER</b><span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Disk Space<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Bandwidth<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> POP3 Email<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Database<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>10</b> Addon Domains<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Instant</b> Backup<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Domain Gratis</b> Selamanya<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited SSL</b> Gratis Selamanya<span></center>
                    </br>
                    <center><button class="btn btn-outline-dark" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Pilih Sekarang</span></button></center>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12" style="line-height: 1.8;margin-top:30px" >
                    <div style="width:100%;line-height: 1.8;border:1px solid #008fee; border-radius:5px;padding-bottom:20px">
                    <div style="width:100%;background-color:#008fee;color:white;">
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Personal</span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em;"><strike>Rp <?php echo number_format($price->personal_old); ?></strike><span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em">Rp.</span><span style="font-family:montserrat-bold;font-size:2em"><?php echo substr($price->personal_new, 0,-3); ?></span><span style="font-family:montserrat-light;font-size:1em"><b>.<?php echo substr($price->personal_new, -3); ?></b>/bln</span></center>
                    </center>
                    </div>
                    <div style="width:100%;background-color:#007fde;color:white;">
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>10.017</b> Pengguna Terdaftar<span></center>
                    </div>
                    <div style="width:100%;">
                    <br/>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>2X RESOURCE POWER</b><span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Disk Space<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Bandwidth<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> POP3 Email<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Database<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Addon Domains<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Instant</b> Backup<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Domain Gratis</b> Selamanya<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited SSL</b> Gratis Selamanya<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Private</b> Name Server<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>SpamAssasin</b> Mail Protection<span></center>
                    </br>
                    <center><button class="btn btn-primary" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Pilih Sekarang</span></button></center>
                    </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12" style="line-height: 1.8;margin-top:30px">
                    <div style="width:100%;line-height: 1.8;border:1px solid #e4e2e2; border-radius:5px;padding-bottom:20px">
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Bisnis</span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em;"><strike>Rp <?php echo number_format($price->bisnis_old); ?></strike><span></center>
                    <center><span style="font-family:montserrat-light;font-size:1em">Rp.</span><span style="font-family:montserrat-bold;font-size:2em"><?php echo substr($price->bisnis_new, 0,-3); ?></span><span style="font-family:montserrat-light;font-size:1em"><b>.<?php echo substr($price->bisnis_new, -3); ?></b>/bln</span></center>
                    </center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>3.552</b> Pengguna Terdaftar<span></center>
                    <br/>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>3X RESOURCE POWER</b><span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Disk Space<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Bandwidth<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> POP3 Email<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Database<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited</b> Addon Domains<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Magic Auto</b> Backup & Restore<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Domain Gratis</b> Selamanya<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Unlimited SSL</b> Gratis Selamanya<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Private</b> Name Server<span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>Prioritas</b> Layanan Support<span></center>
                    <center>
                    <div style="color:#008fee">
                        <span  class="glyphicon glyphicon-star"></span>
                        <span  class="glyphicon glyphicon-star"></span>
                        <span  class="glyphicon glyphicon-star"></span>
                        <span  class="glyphicon glyphicon-star"></span>
                        <span  class="glyphicon glyphicon-star"></span>
                    </div>
                    </center>
                    <center><span style="font-family:montserrat-light;font-size:0.8em;"><b>SpamExpert</b> Pro Mail Protection<span></center>
                    </br>
                    <center><button class="btn btn-outline-dark" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Diskon 40%</span></button></center>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><span style="font-family:montserrat-light;font-size:1.6em">Powerful dengan Limit PHP yang Lebih Besar<span></center>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-12"></div>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <table class="table table-striped">
                        <tr>
                            <td><img src="assets/images/check.png" width="18vw"></td>
                            <td><span style="font-family:montserrat-light;font-size:0.8em;">&nbsp;max execution time 300s<span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;max execution time 300s <span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;php memory limit 1024 MB <span></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <table class="table table-striped">
                        <tr>
                            <td><img src="assets/images/check.png" width="18vw"></td>
                            <td><span style="font-family:montserrat-light;font-size:0.8em;">&nbsp;post max size 128 MB<span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;upload max filesize 128 MB <span></td>
                        </tr>
                        <tr>
                            <td style="padding-top:10px"><img src="assets/images/check.png" width="18vw"></td>
                            <td style="padding-top:10px"><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;max input vars 2500 <span></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12"></div>
            </div>
            <br/>
            <hr style="width:10%;border: 2px solid #e4e2e2;"/>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><span style="font-family:montserrat-light;font-size:1.6em">Semua Paket Hosting Sudah Termasuk<span></center>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_PHP Semua Versi.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">PHP Semua Versi</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Pilih mulai dari versi PHP 5.3 s/d PHP 7<br/>Ubah sesuka Anda!</center>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_My SQL.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">MySQL Versi 5.6</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Nikmati MySQL versi terbaru, tercepat dan kaya akan fitur.</center>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_CPanel.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Panel Hosting cPanel</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Kelola website dengan panel canggih yang familiar di hati Anda.</center>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_garansi uptime.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Garansi Uptime 99.9%</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Data center yang mendukung kelangsungan website Anda 24/7.</center>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_InnoDB.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Database InnoDB Unlimited</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda.</center>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6" style="margin-top:30px">
                    <center><img src="assets/svg/icon PHP Hosting_My SQL remote.svg" style="width:100%;max-width:50px"></center>
                    <br/>
                    <center><span style="font-family:montserrat-bold;font-size:1.5em">Wildcard Remote MySQL</span></center>
                    <center><span style="font-family:montserrat-light;font-size:0.9em;">Mendukung s/d 25 max_user_connections dan 100 max_connections.</center>
                </div>
            </div>
            <br/>
            <hr style="width:10%;border: 2px solid #e4e2e2;"/>
            <br/>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <center><span style="font-family:montserrat-light;font-size:1.6em">Mendukung Penuh Framework Laravel<span></center>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <span style="font-family:montserrat-light;font-size:1.2em">
                            Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda
                        </span>
                        <br/>
                        <br/>
                        <table  style="line-height: 2">
                            <tr>
                                <td><img src="assets/images/check.png" width="18vw"></td>
                                <td><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;Install Laravel <b>1 klik</b> dengan Softaculous Installer. <span></td>
                            </tr>
                            <tr>
                                <td><img src="assets/images/check.png" width="18vw"></td>
                                <td><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;Mendukung ekstensi <b>PHP MCrypt, phar, mbstring, json,</b> dan <b>fileinfo.</b> <span></td>
                            </tr>
                            <tr>
                                <td><img src="assets/images/check.png" width="18vw"></td>
                                <td><span style="font-family:montserrat-light;font-size:0.8em">&nbsp;Tersedia <b>Composer</b> dan <b>SSH</b> untuk menginstall packages pilihan Anda. <span></td>
                            </tr>
                        </table>
                        <br/>
                        <span style="font-family:montserrat-light;font-size:0.7em">
                            Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis
                        </span>
                        <br/>
                        <br/>
                        <button class="btn btn-primary" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Pilih Hosting Anda</span></button>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        &nbsp;
                        <center><img src="assets/svg/illustration banner support laravel hosting.svg" style="width:100%;max-width:400px"></center>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                        <center><span style="font-family:montserrat-light;font-size:1.6em">Modul Lengkap untuk Menjalankan Aplikasi PHP Anda.<span></center>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6" style="text-align: center;">
                    <div style="display: inline-block; text-align: left;">
                        lcePHP<br/>
                        apc<br/>
                        apcu<br/>
                        apm<br/>
                        ares<br/>
                        bcmath<br/>
                        bcompiler<br/>
                        big_int<br/>
                        bitset<br/>
                        bloomy<br/>
                        bz2_filter<br/>
                        clamav<br/>
                        coin_acceptor<br/>
                        crack<br/>
                        dba
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6" style="text-align: center;">
                    <div style="display: inline-block; text-align: left;">
                        http<br/>
                        huffman<br/>
                        idn<br/>
                        igbinary<br/>
                        imagick<br/>
                        imap<br/>
                        inclued<br/>
                        inotify<br/>
                        interbase<br/>
                        intl<br/>
                        ioncube_loader<br/>
                        ioncube_loader_4<br/>
                        jsmin<br/>
                        json<br/>
                        ldap
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6" style="text-align: center;">
                    <div style="display: inline-block; text-align: left;">
                        nd_pdo_mysql<br/>
                        oauth<br/>
                        oci8<br/>
                        odbc<br/>
                        opcache<br/>
                        pdf<br/>
                        pdo<br/>
                        pdo_dblib<br/>
                        pdo_firebird<br/>
                        pdo_mysql<br/>
                        pdo_odbc<br/>
                        pdo_pgsql<br/>
                        pdo_sqlite<br/>
                        pgsql<br/>
                        phalcon
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6" style="text-align: center;">
                    <div style="display: inline-block; text-align: left;">
                        stats<br/>
                        stem<br/>
                        stomp<br/>
                        suhosin<br/>
                        sybase_ct<br/>
                        sysvmsg<br/>
                        sysvsem<br/>
                        sysvshm<br/>
                        tidy<br/>
                        timezonedb<br/>
                        trader<br/>
                        translit<br/>
                        uploadprogress<br/>
                        uri_template<br/>
                        uuid
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <center><button class="btn btn-outline-dark" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Selengkapnya</span></button></center>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <span style="font-family:montserrat-light;font-size:1.6em">Linux Hosting yang Stabil<br/>dengan Teknologi LVE<span>
                    <br/>
                    <br/>
                    <span style="font-family:montserrat-light;font-size:0.6em">
                    SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128GB</b> dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi dengan <b>SSD</b> untuk kecepatan <b>MySQL</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection dan auto backup untuk keamanan website PHP Anda.
                    </span>
                    <br/>
                    <br/>
                    <button class="btn btn-primary" style="border-radius:20px"><span style="font-family:montserrat-bold;font-size:1.4em">Pilih Hosting Anda</span></button>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    &nbsp;
                    <center><img src="assets/images/Image support.png" style="width:100%;max-width:500px"></center>
                </div>
            </div>
        </div>
        <div style="background-color:#e8e8e8;padding-top:20px;padding-bottom:20px;margin-top:30px">
            <div class="container"> 
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6" style="width:100%;">
                        <span style="font-family:montserrat-light;font-size:1em"><b>Bagikan jika Anda menyukai halaman ini.</b><span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6" style="width:100%;">
                    <!-- Widget share -->
                    </div>
                </div>
            </div>
        </div>
        <div style="background-color:#00a2f3;color:white;padding-top:40px;padding-bottom:40px">
            <div class="container"> 
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8" style="width:100%;">
                        <span style="font-family:montserrat-light;font-size:1.6em">Perlu <b>BANTUAN?</b> Hubungi Kami : <b>0247-5305505</b><span>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4" style="width:100%;">
                        <button class="btn btn-primary" style="border-radius:20px;font-family:montserrat-light;font-size:1em;background-color:#00a2f3;border:2px solid white;padding-left:20px;padding-right:20px;float:right"> <span class="glyphicon glyphicon-comment"></span> &nbsp;<b>Live Chat</b></button>
                    </div>
                </div>
            </div>
        </div>
        <div style="background-color:#303030;padding-top:20px;padding-bottom:40px;line-height:1.8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6"  style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>HUBUNGI KAMI</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8">0247-5305505<br/>Senin - Minggu<br/>24 Jam Nonstop</span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8">Jl. Selokan Mataram Monjali<br/>Karangjati MT I/304<br/>Sinduadi, Mlati, Sleman<br/>Yogyakarta 55284</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>LAYANAN</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Domain</a><br/><a href="#" style="color: #d8d8d8">Shared Hosting</a><br/><a href="#" style="color: #d8d8d8">Cloud VPS Hosting</a><br/><a href="#" style="color: #d8d8d8">Managed VPS Hosting</a><br/><a href="#" style="color: #d8d8d8">Web Builder</a><br/><a href="#" style="color: #d8d8d8">Keamanan SSL / HTTPS</a><br/><a href="#" style="color: #d8d8d8">Jasa Pembuatan Website</a><br/><a href="#" style="color: #d8d8d8">Program Affiliasi</a></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>SERVICE HOSTING</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Hosting Murah</a><br/><a href="#" style="color: #d8d8d8">Hosting Indonesia</a><br/><a href="#" style="color: #d8d8d8">Hosting Singapura SG</a><br/><a href="#" style="color: #d8d8d8">Hosting PHP</a><br/><a href="#" style="color: #d8d8d8">Hosting Wordpress</a><br/><a href="#" style="color: #d8d8d8">Hosting Laravel</a></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>TUTORIAL</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Knowledgebase</a><br/><a href="#" style="color: #d8d8d8">Blog</a><br/><a href="#" style="color: #d8d8d8">Cara Pembayaran</a></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>TENTANG KAMI</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Tim Niagahoster</a><br/><a href="#" style="color: #d8d8d8">Karir</a><br/><a href="#" style="color: #d8d8d8">Events</a><br/><a href="#" style="color: #d8d8d8">Penawaran & Promo Spesial</a><br/><a href="#" style="color: #d8d8d8">Kontak Kami</a></span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>KENAPA PILIH NIAGAHOSTER?</b></span>
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.8em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Support Terbaik</a><br/><a href="#" style="color: #d8d8d8">Garansi Harga Termurah</a><br/><a href="#" style="color: #d8d8d8">Domain Gratis Selamanya</a><br/><a href="#" style="color: #d8d8d8">Datacenter Hosting Terbaik</a><br/><a href="#" style="color: #d8d8d8">Review Pelanggan</a></span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>NEWSLETTER</b></span>
                        <br/>
                        <br/>
                        <input type="text" style="width:250px;border-radius:20px"/>
                        <button class="btn btn-primary" style="border-radius:20px;font-family:montserrat-light;font-size:0.8em;background-color:#00a2f3;border:1px solid #00a2f3;">Berlangganan</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:30px">
                        <span style="font-family:montserrat-light;font-size:0.8em;color: #5d5d5d"><b>PEMBAYARAN</b></span>
                        <br/>
                        <br/>
                        <img src="assets/images/payment.png" style="width:100%;max-width:400px">
                        <br/>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.7em;color: #787878">Aktivasi instan dengan e-Payment, Hosting dan domain langsung aktif!</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9" style="margin-top:30px">
                        <span style="font-family:roboto-regular;font-size:0.7em;color: #d8d8d8">Copyright ©2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta</span>
                        <br/>
                        <span style="font-family:roboto-regular;font-size:0.7em;color: #d8d8d8">Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3" style="margin-top:30px;text-align:right">
                        <span style="font-family:roboto-regular;font-size:0.7em;color: #d8d8d8"><a href="#" style="color: #d8d8d8">Syarat dan Ketentuan | Kebijakan Privasi</a></span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>